package web

import (
	"localnetwork/db"
	"net/http"
	"time"

	"gitlab.com/gbh007/gojlog"
	//Надо
	_ "gitlab.com/krasecology/go-lib"
	"gitlab.com/krasecology/go-lib/web"
)

// addSample получает данные в формате json и подготавливает к записи в базу
func addSample() http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		decoded := []db.Data{}
		if web.ParseJSON(r, &decoded) != nil {
			web.SetError(r, web.ErrParseData)
			//gojlog.Error(web.GetAccountInfo(r), err) тут вставить статистику
			return
		}
		if db.Insert(decoded) != nil {
			web.SetError(r, web.ErrCreateData)
			return
		}
		web.SetResponse(r, struct{}{})
	})
}

// getPostsInfo получает данные по посту (с сенсорами) и отдает фронту
func getPostsInfo() http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		web.SetResponse(r, db.PostsData())
	})
}

func getData() http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		req := struct {
			PostID   int `json:"post_id"`
			SensorID int `json:"sensor_id"`
			PeriodID int `json:"period_id"`
		}{}

		if web.ParseJSON(r, &req) != nil {
			web.SetError(r, web.ErrParseData)
			return
		}

		web.SetResponse(r, db.GraphData(req.PostID, req.SensorID, req.PeriodID))
	})
}

// Run запускает веб-сервер
func Run(addr string, staticDir string) <-chan struct{} {
	mux := http.NewServeMux()
	mux.Handle("/", http.FileServer(http.Dir(staticDir)))
	web.AddHandler(mux, "/api/send-sample/json", "local-network:input", addSample())
	web.AddHandler(mux, "/api/get/posts", "", getPostsInfo())
	web.AddHandler(mux, "/api/get/data", "", getData())

	server := &http.Server{
		Addr:         addr,
		Handler:      mux,
		ReadTimeout:  1 * time.Minute,
		WriteTimeout: 1 * time.Minute,
		IdleTimeout:  1 * time.Minute,
	}
	done := make(chan struct{})
	go func() {
		if err := server.ListenAndServe(); err != nil {
			gojlog.Error(err)
		}
		close(done)
	}()
	return done
}
