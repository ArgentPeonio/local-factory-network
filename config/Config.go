package config

import (
	"encoding/json"
	"os"

	"gitlab.com/gbh007/gojlog"
)

// Config структура для хранения конфигурации подключения к бд
type Config struct {
	Postgres struct {
		URI      string `json:"uri"`
		User     string `json:"user"`
		Password string `json:"password"`
		DBName   string `json:"dbname"`
		Port     int    `json:"port"`
	} `json:"postgres"`
	Mongo struct {
		URI      string `json:"uri"`
		User     string `json:"user"`
		Password string `json:"password"`
		DBName   string `json:"dbname"`
	} `json:"mongo"`
	Log gojlog.LogConfig `json:"log"`
}

var _config Config

// GetConfig возвращает объект конфигурации
func GetConfig() Config {
	return _config
}

// Load загружает конфиг
func Load(configPath string) error {
	file, err := os.Open(configPath)
	if err != nil {
		return err
	}
	defer file.Close()
	d := json.NewDecoder(file)
	if err := d.Decode(&_config); err != nil {
		return err
	}
	gojlog.SetConfig(_config.Log)
	return nil
}
