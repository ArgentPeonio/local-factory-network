package db

import (
	"time"

	"gitlab.com/gbh007/gojlog"
)

// Data структура для данных, полученных со стороны для записи в базу
type Data struct {
	PostID   int       `json:"Post"`
	SensorID int       `json:"Sensor"`
	Date     time.Time `json:"Data"`
	Treshold float64   `json:"Normative"`
	Value    float64   `json:"Value"`
}

// Insert - запись полученных данных в базу
func Insert(decode []Data) error {
	sqlStatment := `INSERT INTO main.data (post_id, sensor_id, value, treshold, "date")
	values ($1, $2, $3, $4, $5)`

	for _, record := range decode {
		_, err := _db.Exec(sqlStatment, record.PostID, record.SensorID, record.Value, record.Treshold, record.Date)
		if err != nil {
			gojlog.Error(err)
			return err
		}
	}
	return nil
}
