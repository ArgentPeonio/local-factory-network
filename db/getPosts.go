package db

import "gitlab.com/gbh007/gojlog"

var sensorMap = map[int][]int{
	1: {11},
	2: {1, 2, 3, 4, 5, 8, 10},
	3: {1, 2, 3, 4, 5},
	4: {1, 2, 3, 4, 5},
	5: {1, 2, 3, 4, 5},
	6: {1, 2, 3, 4, 5},
	7: {1, 2, 3, 4, 5},
	8: {1, 2, 3, 4, 5},
	9: {11},
}

// Sensors информация о сенсорах - айди и название
type Sensors struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// Area информация о конкретной площадки, главным образом сделано для СГК
type Area struct {
	ID        int        `json:"id"`
	Name      nullString `json:"name"`
	ShowDaily bool       `json:"show_daily"`
	Sensors   []Sensors  `json:"sensors"`
}

// PostInfo содержит в себе имя поста и предыдущую структуру
type PostInfo struct {
	Name string `json:"name"`
	Area []Area `json:"area"`
}

// PostsData получает все данные по постам и сенсорам
func PostsData() (pairs []PostInfo) {

	type post struct {
		ID        int        `json:"id"`
		Name      string     `json:"name"`
		Area      nullString `json:"area"`
		ShowDaily bool       `json:"show_daily"`
	}

	MapedData := make(map[string][]Area)
	postsRequest := `SELECT id, "name", area, show_daily 
					FROM main.posts p 
					WHERE visible IS TRUE 
					ORDER BY sort_order`
	p, err := _db.Query(postsRequest)
	if err != nil {
		gojlog.Error(err)
		return nil
	}

	sensorsRequest := `SELECT id, "name"
					FROM main.sensors s 
					WHERE visible IS TRUE 
					ORDER BY sort_order`
	s, err := _db.Query(sensorsRequest)
	if err != nil {
		gojlog.Error(err)
		return nil
	}
	// Тут в мапу можно записать. Должно помочь минуснуть один фор ниже. Подумать.
	var sensorsData []Sensors
	for s.Next() {
		y := Sensors{}
		err := s.Scan(&y.ID, &y.Name)
		if err != nil {
			gojlog.Error(err)
			return nil
		}
		sensorsData = append(sensorsData, y)
	}
	for p.Next() {
		i := post{}
		err := p.Scan(&i.ID, &i.Name, &i.Area, &i.ShowDaily)
		if err != nil {
			gojlog.Error(err)
			return nil
		}
		var tmp []Sensors
		for _, y := range sensorsData {
			b := 0
			for x := range sensorMap {
				if b >= len(sensorMap[i.ID]) {
					break
				} else {
					if sensorMap[i.ID][b] == y.ID {
						tmp = append(tmp, Sensors{
							ID:   y.ID,
							Name: y.Name,
						})
					}
				}
				b++
				x++
			}
		}
		MapedData[i.Name] = append(MapedData[i.Name], Area{
			ID:        i.ID,
			Name:      i.Area,
			ShowDaily: i.ShowDaily,
			Sensors:   tmp,
		})

	}
	pairs = []PostInfo{}
	for key, value := range MapedData {
		pairs = append(pairs, PostInfo{key, value})
	}
	return
}
