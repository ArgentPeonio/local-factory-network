package db

import (
	"time"

	"gitlab.com/gbh007/gojlog"
)

// RequestedData структура для данных с базы для графиков
type RequestedData struct {
	Value    float64   `json:"value"`
	Treshold float64   `json:"treshold"`
	Date     time.Time `json:"date"`
}

// GraphData получает данные для графика с базы по айди поста, сенсора и выбранного периода
func GraphData(PostID, SensorID, PeriodID int) (rd []RequestedData) {
	rd = make([]RequestedData, 0)

	var dtEnd time.Time
	dtNow := time.Now()

	switch PeriodID {
	case 1:
		dtEnd = dtNow.Add(-24 * time.Hour)
	case 2:
		dtEnd = dtNow.Add(-168 * time.Hour)
	case 3:
		dtEnd = dtNow.Add(-744 * time.Hour)
	case 4:
		dtEnd = dtNow.Add(-8760 * time.Hour)
	}

	// < - dtNow; > - dtEnd
	dataRequest := `SELECT da.value, da.treshold, da."date" 
					FROM main."data" da
					WHERE da.post_id = $1 
					AND da.sensor_id = $2 
					AND da."date" < $3 
					AND da."date" > $4
					AND da.valid IS TRUE
					ORDER BY da."date"`

	d, err := _db.Query(dataRequest, PostID, SensorID, dtNow, dtEnd)
	if err != nil {
		gojlog.Error(err)
		return nil
	}

	for d.Next() {
		re := RequestedData{}
		err := d.Scan(&re.Value, &re.Treshold, &re.Date)
		if err != nil {
			gojlog.Error(err)
			return nil
		}
		rd = append(rd, re)
	}
	return
}
