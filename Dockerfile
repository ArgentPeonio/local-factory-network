FROM amd64/alpine:3.13
LABEL maintainer="Алина Вострикова alina.vostrikova24@gmail.com"
# добавление ssl сертификатов и пакета временых зон
RUN apk update && apk add ca-certificates tzdata
RUN mkdir /app
COPY main /app/
COPY frontend/build /app/static
WORKDIR /app
EXPOSE 80
ENTRYPOINT ["/app/main", "-c", "/app/cnf/config.json"]
