//import logo from './logo.svg';
import { Component } from 'react';
import './App.css';
import { Load } from './base/load';
import { Chart, } from 'chart.js';
import React from 'react';
import "./style.css";



class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedPost: {},
      selectedSensors: {},
      posts: [],
      graphData: [],
      selected: 2,
      selectedSens: 1,
      dispPeriodId: 1,
      selectedPeriod: 2,
      showDaily: false,
      name: "",
      periods: [{ id: 1, data: "День" },
      { id: 2, data: "Неделя" },
      { id: 3, data: "Месяц" },
      { id: 4, data: "Год" }],
      periods2: []
    };
  }
  componentDidMount() {
    new Load(this.context, "Получение постов", "/api/get/posts")
      .SetErr()
      .Load()
      .then((posts) => {
        this.setState({
          name: posts[0].name,
          posts: posts,
          selectedPost: posts[0].area,
          selectedSensors: posts[0].area[0].sensors,
          selected: posts[0].area[0].id,
          showDaily: posts[0].area[0].show_daily
        });
      });
    this.get();
    this.sensButton(this.state.selectedSensors)
    this.periodCheck()
  }

  componentDidUpdate(_, prevState) {
    if (this.state.selectedPeriod !== prevState.selectedPeriod ||
      this.state.selected !== prevState.selected ||
      this.state.selectedSens !== prevState.selectedSens) {
      this.get();
      if (this.state.showDaily !== prevState.showDaily){
      this.periodCheck();
    }
    }
  }

  get() {
    new Load(this.context, "Получение данных", "/api/get/data", { post_id: this.state.selected, sensor_id: this.state.selectedSens, period_id: this.state.selectedPeriod })
      .SetErr()
      .Load()
      .then((graphData) => {
        this.setState({ graphData: graphData });
      });
  }

  periodCheck() {
    if (this.state.showDaily){
      return this.setState({periods2: this.state.periods})
    } else { 
      return this.setState({periods2: this.state.periods.slice(1)})
    }
  }

  sensButton(selectedSensors) {
     return Object.keys(selectedSensors).map(key => (
        <li key={selectedSensors[key].id}
          style={{
            background: this.state.selectedSens === selectedSensors[key].id ? "#656E5A" : "none",
            color: this.state.selectedSens === selectedSensors[key].id ? "#ffffff" : "black",
            cursor: "pointer",
            padding: "3px",
            width: "100%",
          }}
          onClick={() => this.setState({ selectedSens: selectedSensors[key].id })}>{selectedSensors[key].name}</li>
      ))
  }

  render() {
    const selectedPost = this.state.selectedPost;
    const posts = this.state.posts;
    const selectedSensors = this.state.selectedSensors;
    var periods2 = this.state.periods2;
    const graphData = this.state.graphData;
    const name = this.state.name;

    

    return (      
      <div className="main">
        <div className="general">
          <div className="posts">
            {/*Отрисовка постов, по идее должен еще и сразу рисовать график к русалу*/}
            {posts.map((post) => (
              <button key={post.name}
              style={{
                background: post.name === name ? "#656E5A" : "#ffffff",
                color: post.name === name ? "#ffffff" : "black",
              }} 
              onClick={() => {
                this.setState({
                  name: post.name,
                  selectedPost: post.area,
                  selectedSensors: post.area[0].sensors,
                  selected: post.area[0].id,
                  showDaily: post.area[0].show_daily
                })
              }}>{post.name}</button>
            ))}</div>

          {/*Выбор труб для ТЭЦ*/}
          <div className="area">
            {Object.keys(selectedPost).length <= 1 ? null :
              <ul style={{ listStyle: "none", textAlign: "center", marginBlockStart: "0", marginBlockEnd: "0" }}>
                {Object.keys(selectedPost).map(key => (
                  <li key={selectedPost[key].id}
                    style={{
                      background: this.state.selected === selectedPost[key].id ? "#656E5A" : "none",
                      color: this.state.selected === selectedPost[key].id ? "#ffffff" : "black",
                      cursor: "pointer",
                      display: "inline",
                      padding: "3px",
                    }}
                    onClick={() => this.setState({
                      selected: selectedPost[key].id,
                      selectedSensors: selectedPost[key].sensors,
                      showDaily: selectedPost[key].show_daily
                    })}>{selectedPost[key].name}</li>
                ))}
              </ul>}</div>

          {/*Список веществ*/}
          <div className="sensors">
            <ul style={{ listStyle: "none", padding: "10px", }}>
              <li style={{ fontWeight: "bold" }}>Вещества</li>
                    {this.sensButton(selectedSensors)}
            </ul></div>

          {/*Периоды отрисовки*/}
          <div className="periods">
            <ul style={{ listStyle: "none", padding: "10px", }}>
              <li style={{ fontWeight: "bold" }}>Периоды</li>
              {Object.keys(periods2).map(key => (
                <li key={periods2[key].id}
                  style={{
                    background: this.state.selectedPeriod === periods2[key].id ? "#656E5A" : "none",
                    color: this.state.selectedPeriod === periods2[key].id ? "#ffffff" : "black",
                    cursor: "pointer",
                    padding: "3px",
                    width: "100%",
                  }}
                  onClick={() => this.setState({ selectedPeriod: periods2[key].id })}>{periods2[key].data}</li>
              ))}
            </ul></div>

          {/*График (вау)*/}
          <div className="chart">
            <Graph
              value={graphData}
              func={(e) => prepData(e)}
              type="line"
            /></div>
          <br />
        </div>

      </div>
    );
  }
}

class Graph extends React.Component {
  constructor(props) {
    super(props);
    this.graph = null;
    this.graphRef = React.createRef();
  }
  componentDidMount() {
    this.graph = new Chart(this.graphRef.current, {
      type: this.props.type,
    });
  }
  componentDidUpdate(prevProps) {
    if (this.props.value && this.props.value !== prevProps.value) {
      console.log(this.props.func(this.props.value));
      this.graph.data = this.props.func(this.props.value);
      this.graph.update();
    }
  }
  render() {


    return (
      <div>
        {this.props.head}
        <div style={{ position: "centre", height: "100%", width: "100%", backgroundColor: "white", borderRadius: "10px" }}>
          <canvas ref={this.graphRef}></canvas>
        </div>

      </div>
    );
  }
}

function prepData(value) {
  let graphDataLabel = [];
  let graphValue = [];
  let graphTreshold = [];
  let datasets = [];
  value.forEach(a => {
    graphDataLabel.push(new Date(a.date).toLocaleString());
    graphValue.push(a.value);
    graphTreshold.push(a.treshold);

  })
  datasets.push({
    label: "Показатель, г/с",
    data: graphValue,
    borderColor: "#3F88C5",
    fill: false,
    tension: 0.1,
    pointRadius: 0
  }, {
    label: "Норматив, г/с",
    data: graphTreshold,
    borderColor: "#F64740",
    fill: false,
    tension: 0.1,
    pointRadius: 0
  }
  )
  return {
    datasets,
    labels: graphDataLabel,
  };
}
export default App;
